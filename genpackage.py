#!/usr/bin/python3

import argparse
import glob
import os
import yaml

from jinja2 import Template

config = yaml.safe_load(open('genpackage.yaml'))

def fill_template(inf, outf, lang=None):
    global config
    infile = open(inf)
    template_text = ''
    for l in infile:
        template_text = template_text + l
    t = Template(template_text)
    outfile = open(outf, 'w')
    nblangs = len(config['languages'])
    outfile.write(t.render(lang_list=config['languages'], lang=lang, nblangs=nblangs))

def generate():
    fill_template('debian/control.in', 'debian/control')
    fill_template('debian/kali-ci.yml.in', 'debian/kali-ci.yml')

    for l in config['languages']:
        fill_template('debian/firefox-developer-edition-kbx.install.in',
                      'debian/firefox-developer-edition-%s-kbx.install' % (l.lower(),), l)
        fill_template('Dockerfile.in',
                      'firefox-developer-edition-%s.Dockerfile' % (l.lower(),), l)
        fill_template('kaboxer.yaml.in',
                      'firefox-developer-edition-%s.kaboxer.yaml' % (l.lower(),), l)
        fill_template('kaboxer-firefox-developer-edition.desktop.in',
                      'kaboxer-firefox-developer-edition-%s.desktop' % (l.lower(),), l)


def clean():
    globs = [
        'debian/firefox-developer-edition-*-kbx.install',
        'firefox-developer-edition-*.Dockerfile',
        'firefox-developer-edition-*.kaboxer.yaml',
        'kaboxer-firefox-developer-edition-*.desktop',
    ]
    for g in globs:
        for f in glob.glob(g):
            os.unlink(f)

parser = argparse.ArgumentParser()

subparsers = parser.add_subparsers(title='subcommands', help='action to perform', dest='action', required=True)

parser_generate = subparsers.add_parser('generate', help='generate files')
parser_generate.set_defaults(func=generate)

parser_clean = subparsers.add_parser('clean', help='clean files')
parser_clean.set_defaults(func=clean)

args = parser.parse_args()
args.func()

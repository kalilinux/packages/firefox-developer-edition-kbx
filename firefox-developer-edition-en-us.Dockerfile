FROM debian:stable-slim AS builder
ARG KBX_APP_VERSION=latest
RUN apt update \
 && apt install -y bzip2 dpkg-dev wget
COPY download-firefox.sh /download-firefox.sh
RUN /download-firefox.sh $KBX_APP_VERSION en-US \
 && tar xaf firefox.tar.bz2

FROM debian:stable-slim AS runner
RUN apt update \
 && apt install -y fonts-noto fonts-noto-cjk libasound2 libdbus-glib-1-2 libgtk-3-0 libx11-xcb1 libxt6
COPY --from=builder /firefox /firefox
COPY policies.json /firefox/distribution/policies.json
RUN mkdir -p /kaboxer \
 && awk -F= '/^Version=/ { print $2 }' /firefox/application.ini > /kaboxer/version